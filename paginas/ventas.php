<?php 
session_start();


if (!isset($_SESSION['usuario'])){

header("location:../");

}


 ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title> Lista de productos - Sistema Farmacéutico</title>

    <link href="../lib/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../lib/css/bootstrap-theme.min.css">

    <!-- Custom CSS -->
    <link href="../lib/css/sb-admin.css" rel="stylesheet">


    <!-- Custom Fonts -->
  <script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <?php include('includes/navbar.php');?>


        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header" align="center"> Venta de medicamentos  <i class="fa fa-shopping-cart" style="font-size:25px" ></i> </h1>
                    </div>
                </div>
                <!-- /.row -->

                
               		<br><br>


               <div id="all-item">
                <div class="form-group">
                    <label class="control-label col-sm-1" for="cliente_vent">Cliente:</label>
                    <div class="col-sm-4"> 
                      <input type="text" class="form-control" id="cliente_vent" placeholder="Ingrese el cliente">
                    </div>
                  </div>
                 <div class="form-group">
                    <label class="control-label col-sm-2" for="metodo_vent">Metodo de pago:</label>
                    <div class="col-sm-4"> 
                        <select name="" id="metodo_vent" class="form-control">
                            <option value="">---Seleccione metodo---</option>
                            <option value="1">Contado</option>
                            <option value="2">Credito</option>
                        </select>
                    </div>
                  </div> 
<br><br><br>
                 <div style="margin-left: 350px">

                    <div class="col-sm-4"> 
                        <button class="btn btn-secondary" data-toggle="modal" data-target="#agregar_vent">Inventario <i class="fa fa-book"></i></button> 
                        <button class="btn btn-primary" onclick="imprimir_todo()">Imprimir <i class="fas fa-print"></i></button> 
                    </div>

                  </div> 
                  
               
<br><br>
                <table class="table ">
                    <thead class="thead-table">
                    <tr >
                        <th align="center">Codigo</th>
                        <th>Nombre</th>
                        <th>Cantidad</th>
                        <th>Precio</th>
                        <th colspan="2"></th>
                        <th>Borrar</th>
                    </tr>
                    </thead>
                    <tbody class="mostrar_ventas">

                      <tr>
                          <td colspan="4"></td>
                          <td > <h4>Sub total:</h4></td>
                          <td ><h4>$0</h4></td>
                      </tr>
                     <tr>
                          <td colspan="4"></td>
                          <td > <h4>Total:</h4></td>
                          <td ><h4>$0</h4></td>
                      </tr>
                    </tbody>

                </table>


               </div>

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<!-- PHP -->
<?php include('modals/cambiar_pass.php'); ?>
<?php include('modals/agregar_venta.php'); ?>
<?php include('modals/selec_prod_venta.php'); ?>
<!-- PHP -->

    <script type="text/javascript" src="../lib/js/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="../lib/js/jquery-1.12.3.js"></script>
    <script type="text/javascript" src="../lib/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/ctrlVentas.js"></script>

  
</body>

</html>
