<?php 
session_start();


if (!isset($_SESSION['usuario'])){

header("location:../");

}


 ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title> Lista de productos - Sistema Farmacéutico</title>

   <!-- Bootstrap Core CSS -->
    <link href="../lib/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../lib/css/bootstrap-theme.min.css">

    <!-- Custom CSS -->
    <link href="../lib/css/sb-admin.css" rel="stylesheet">

 

    <!-- Custom Fonts -->
  <script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>


</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <?php include('includes/navbar.php');?>


        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header" align="center"> Lista de paquetes <i class="fa fa-clipboard-list" style="font-size:25px" ></i></h1>
                    </div>
                </div>
                <!-- /.row -->
                <button class="btn btn-primary " data-toggle="modal" data-target="#agregar_inventario">Agregar inventario
						<i class="fa fa-plus-circle"></i>
                </button>

	                <div style="float: right;">
	                	<input type="text" placeholder="Buscar por codigo" id="buscar_Pack" onkeyup="buscar_Paquetes()" class="form-control">
	                </div>				



               <div id="all-item">
               		<br><br>
               	<table class="table table-bordered ">
               		<thead class="thead-table">
               			<tr>
               				<th scope="col">Codigo</th>
               				<th scope="col">Nombre</th>
               				<th scope="col">Categoria</th>
               				<th scope="col">Fabricante</th>
               				<th scope="col">Fabricado</th>
               				<th scope="col">Comprado</th>
               				<th scope="col">Precio</th>
               				<th scope="col">Cantidad</th>
               				<th scope="col">Vence</th>
               				<th scope="col">Acciones</th>
               			</tr>
					</thead>
					<tbody class="mostrar_paquetes">


				    </tbody>
               	</table>



               </div>

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->




<!-- PHP -->

<?php include('modals/agregar_inventario.php'); ?>
<?php include('modals/cambiar_pass.php'); ?>

<!-- PHP -->

    <script type="text/javascript" src="../lib/js/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="../lib/js/jquery-1.12.3.js"></script>
    <script type="text/javascript" src="../lib/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/ctrlProductos.js"></script>
    <script type="text/javascript" src="../js/ctrlLogin.js"></script>
    <script type="text/javascript">
          $(document).ready(function(){

            $('#productos').addClass("active")
            $('#productos_perfil').addClass("activo")
           })

    </script>
</body>

</html>
