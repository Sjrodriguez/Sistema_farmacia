<div class="modal fade" id="agregar_inventario" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog " role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">Agregar Inventario</h4>
			</div>
			<div class="modal-body">
					
						<form class="form-horizontal">
				  <div class="form-group">
				    <label class="control-label col-sm-3" for="producto_inv">Producto:</label>
				    <div class="col-sm-8">
				     	<select name="producto" id="producto_inv"  class="form-control producto_input">

				     	</select>
				    </div>
				  </div>

				  <div class="form-group">
				    <label class="control-label col-sm-3" for="cantidad_inv">Cantidad:</label>
				    <div class="col-sm-8"> 
				      <input type="number" class="form-control" id="cantidad_inv" placeholder="Ingrese la cantidad">
				    </div>
				  </div>
				  <div class="form-group">
				    <label class="control-label col-sm-3" for="precio_inv">Precio de compra:</label>
				    <div class="col-sm-8"> 
				      <input type="number" class="form-control" id="precio_inv" placeholder="Ingrese la precio">
				    </div>
				  </div>
				  <div class="form-group">
				    <label class="control-label col-sm-3" for="fabricante_inv">Fabricante:</label>
				    <div class="col-sm-8"> 
				      <input type="text" class="form-control" id="fabricante_inv" placeholder="Ingrese el fabricante">
				    </div>
				  </div>

				  <div class="form-group">
				    <label class="control-label col-sm-3" for="fabricado_inv">Fabricado:</label>
				    <div class="col-sm-8"> 
				      <input type="date" class="form-control" id="fabricado_inv" format="dd/MM/yyyy" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" placeholder="Fecha de fabricacion">
				    </div>
				  </div>

				  <div class="form-group">
				    <label class="control-label col-sm-3" for="comprado_inv">Comprado:</label>
				    <div class="col-sm-8"> 
				      <input type="date" class="form-control" id="comprado_inv" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" placeholder="Fecha comprado">
				    </div>
				  </div>

				  <div class="form-group">
				    <label class="control-label col-sm-3" for="vence_inv">Vencimiento:</label>
				    <div class="col-sm-8"> 
				      <input type="date" class="form-control" id="vence_inv" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" placeholder="Fecha de vencimiento">
				    </div>
				  </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" id="btn_agregar_inventario">Guardar  <i class="fa fa-save"></i></button>
      </div>
				</form>


			</div>
		</div>
	</div>
</div>