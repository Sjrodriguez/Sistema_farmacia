<div class="modal fade" id="cambiar_pass" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">Cambiar contraseña</h4>
			</div>
			<div class="modal-body">
					
			<form class="form-horizontal">
				  <div class="form-group">
				    <label class="control-label col-sm-3" for="new_pass">Nueva contraseña :</label>
				    <div class="col-sm-8"> 
				      <input type="password" class="form-control" id="new_pass" placeholder="Nueva contraseña">
				    </div>
				  </div>
				  <div class="form-group">
				    <label class="control-label col-sm-3" for="new_pass_confirm">Confirmar contraseña  :</label>
				    <div class="col-sm-8"> 
				      <input type="password" class="form-control" id="new_pass_confirm" placeholder="Confirmar contraseña">
				    </div>
				  </div>


      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" id="cambio_pass">Guardar cambios<i class="fa fa-save"></i></button>
      </div>
				</form>


			</div>
		</div>
	</div>
</div>