<div class="modal fade" id="modificar_usuario" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog " role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">Modificar Usuario</h4>
			</div>
			<div class="modal-body">
					
			<form class="form-horizontal">
				  <div class="form-group">
				    <label class="control-label col-sm-3" for="nombre_editar_usu">Nombre :</label>
				    <div class="col-sm-8"> 
				      <input type="text" class="form-control" id="nombre_editar_usu" placeholder="Nombre">
				    </div>
				  </div>
				  <div class="form-group">
				    <label class="control-label col-sm-3" for="apellido_editar_usu">Apellido :</label>
				    <div class="col-sm-8">
				      <input type="text" class="form-control" id="apellido_editar_usu" placeholder="Apellido">
				    </div>
				  </div>

				  <div class="form-group">
				    <label class="control-label col-sm-3" for="telefono_editar_usu">Telefono :</label>
				    <div class="col-sm-8">
				      <input type="number" class="form-control" id="telefono_editar_usu" placeholder="Telefono">
				    </div>
				  </div>

				  <div class="form-group">
				    <label class="control-label col-sm-3" for="usuario_editar_usu">Usuario :</label>
				    <div class="col-sm-8"> 
				      <input type="text" class="form-control" id="usuario_editar_usu" placeholder="Nombre de usuario">
				    </div>
				  </div>
<!-- 
				  <div class="form-group">
				    <label class="control-label col-sm-3" for="contrasena_editar_usu">Contraseña :</label>
				    <div class="col-sm-8"> 
				      <input type="password" class="form-control" id="contrasena_editar_usu" placeholder="Contraseña">
				    </div>
				  </div>

				  <div class="form-group">
				    <label class="control-label col-sm-3" for="confirmar_editar_contra">Confirmar contraseña :</label>
				    <div class="col-sm-8"> 
				      <input type="password" class="form-control" id="confirmar_editar_contra" placeholder="Confirmar contraseña">
				    </div>
				  </div>
 -->
				  <div class="form-group">
				    <label class="control-label col-sm-3" for="direccion_editar_usu">Direccion :</label>
				    <div class="col-sm-8"> 
				      <input type="text" class="form-control" id="direccion_editar_usu" placeholder="Direccion">
				    </div>
				  </div>
				  <div class="form-group ocult_tipo" >
				    <label class="control-label col-sm-3" for="tipo_editar_usu">Tipo de usuario :</label>
				    <div class="col-sm-8"> 
				     	<select name="" id="tipo_editar_usu" class="form-control">
				     		<option value="">-- Seleccione -- </option>
				     		<option value="1">Administrador</option>
				     		<option value="2">Normal</option>
				     	</select>
				    </div>
				    <input type="hidden" id="id_usu_o">
				  </div>
		
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" id="boton_modificar_usu" >Modificar usuario <i class="fa fa-save"></i></button>
      </div>
				</form>


			</div>
		</div>
	</div>
</div>