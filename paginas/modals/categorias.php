<div class="modal fade" id="categorias" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog  " role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">Categorias</h4>
			</div>
			<div class="modal-body">
					
			<form class="form-horizontal">
				  <div class="form-group">
				    <label class="control-label col-sm-2" for="nombre_cat">Nombre :</label>
				    <div class="col-sm-4"> 
				      <input type="text" class="form-control" id="nombre_cat" placeholder="Nombre">
				    </div>

				    <label class="control-label col-sm-2" for="descripcionn_cat">Descripcion:</label>
				    <div class="col-sm-4">
				      <input type="text" class="form-control" id="descripcionn_cat" placeholder="Descripcion">
				    </div>
				  </div>

      <div class="modal-footer">
      	<div style="float: left;">
      	  <button type="button" class="btn btn-primary" id="boton_agregar_cat">Guardar categoria <i class="fa fa-save"></i></button>
      		
      	</div>
	     <div style="float: right;">
	     	<input type="text" placeholder="Buscar categoria" onkeyup="buscar_categoria()" id="buscar_cat" class="form-control">
	      </div>	
      </div>
				</form>
               	<table class="table table-bordered ">
               		<thead class="thead-table">
               			<tr>
               				<th scope="col">Nombre</th>
                     		 <th scope="col">Descripcion</th>
               				<th scope="col" ></th>
               			</tr>
					</thead>					
					<tbody class="mostrar_categorias">

					</tbody>
				</table>

			</div>
		</div>
	</div>
</div>