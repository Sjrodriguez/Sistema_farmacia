<div class="modal fade" id="modificar_producto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog " role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">Modificar Producto</h4>
			</div>
			<div class="modal-body">
					
			<form class="form-horizontal">
				  <div class="form-group">
				    <label class="control-label col-sm-3" for="codigo_editar_pro">Codigo:</label>
				    <div class="col-sm-8"> 
				      <input type="text" class="form-control" id="codigo_editar_pro" placeholder="Ingrese el codigo">
				    </div>
				  </div>

				  <div class="form-group">
				    <label class="control-label col-sm-3" for="nombre_editar_pro">Nombre del producto:</label>
				    <div class="col-sm-8">
				      <input type="text" class="form-control" id="nombre_editar_pro" placeholder="Nombre del producto">
				    </div>
				  </div>

				  <div class="form-group">
				    <label class="control-label col-sm-3" for="precio_editar_pro">Precio:</label>
				    <div class="col-sm-8"> 
				      <input type="number" class="form-control" id="precio_editar_pro" placeholder="Ingrese el precio">
				    </div>
				  </div>

				  <div class="form-group">
				    <label class="control-label col-sm-3" for="unidad_editar_pro">Unidad:</label>
				    <div class="col-sm-8"> 
				      <input type="text" class="form-control" id="unidad_editar_pro" placeholder="Unidad de medida">
				    </div>
				  </div>

				  <div class="form-group">
				    <label class="control-label col-sm-3" for="descripcion_editar_pro">Descripcion:</label>
				    <div class="col-sm-8"> 
				      <input type="text" class="form-control" id="descripcion_editar_pro" placeholder="Descripcion">
				    </div>
				  </div>

				  <div class="form-group">
				    <label class="control-label col-sm-3" for="categoria_editar_pro">Categoria:</label>
				    <div class="col-sm-8"> 
				     	<select name="" id="categoria_editar_pro" class="form-control categorias_input">
				     	</select>
				    </div>
				    <input type="hidden" id="id_pro_o">
				  </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" id="boton_modificar_producto">Modificar producto <i class="fa fa-save"></i></button>
      </div>
				</form>


			</div>
		</div>
	</div>
</div>