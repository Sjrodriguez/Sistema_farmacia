<div class="modal fade" id="agregar_vent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog " role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel"> <i class="fas fa-plus-square"></i>Agregar </h4>
			</div>
			<div class="modal-body">
					
			<form class="form-horizontal">
				  <div class="form-group">
				    <label class="control-label col-sm-3" for="codigo_ven">Cod. Producto:</label>
				    <div class="col-sm-7"> 
				      <input type="text" class="form-control" id="codigo_ven" placeholder="00001">
				    </div>
				    
					<button type="button" class="btn btn-secondary" data-dismiss="modal" data-toggle="modal" data-target="#selec_prod"><i class="fas fa-search"></i></button>
				  </div>

				  <div class="form-group">
				    <label class="control-label col-sm-3" for="nombre_ven">Producto:</label>
				    <div class="col-sm-8">
				      <input type="text" class="form-control"  disabled="disabled" id="nombre_ven" placeholder="Nombre del producto">
				    </div>
				  </div>

				  <div class="form-group">
				    <label class="control-label col-sm-3"  for="precio_ven">Precio:</label>
				    <div class="col-sm-8"> 
				      <input type="number" class="form-control"  disabled="disabled" id="precio_ven" placeholder="Pecio">
				    </div>
				  </div>

				  <div class="form-group">
				    <label class="control-label col-sm-3" for="cantidad_ven">Cantidad:</label>
				    <div class="col-sm-8"> 
				      <input type="number" class="form-control" id="cantidad_ven" placeholder="Cantidad">
				    </div>
				  </div>


				  <div class="form-group">
				    <label class="control-label col-sm-3" for="unidad_ven">Unidad:</label>
				    <div class="col-sm-8"> 
				      <input type="text" class="form-control"  disabled="disabled" id="unidad_ven" placeholder="Unidad de medida">
				    </div>
				  </div>





      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" id="boton_agregar_pro" onclick="confirmar_codigo()">Agregar </button>
      </div>
				</form>


			</div>
		</div>
	</div>
</div>