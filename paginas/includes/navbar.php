<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="home.php">Sistema Farmacéutico</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php  echo $_SESSION['nick']; ?> <i class="fa fa-caret-down"></i> </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a class="aqui" data-toggle="modal" data-target="#cambiar_pass"><i class="fa fa-key"></i> Contraseña</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="aqui" id="cerrar_session"><i class="fa fa-fw fa-power-off"> </i> Salir</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class=" collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav prueba">
                    <li id="inicio">
                        <a href="home.php"><i class="fa fa-home"></i> Inicio</a>
                    </li>
                    <li id="usuario">
                        <a href="usuarios.php" id="traerUsu"> <i class="fa fa-users"></i> Usuarios</a>
                    </li>
                    <li id="productos">
                        <a > <i class="fa fa-warehouse "></i> Productos</a>
                        <ul >
                             <li id="productos_list">
                                <a href="lista_productos.php" "> <i class="fa fa-list-alt"></i> Lista de productos</a>
                            </li>
                            <li id="productos_perfil">
                                <a href="perfil_producto.php"> <i class="fa fa-clipboard-list"></i> Paquetes </a>
                            </li>
                            <li id="productos_inv">
                                <a href="inventario.php"> <i class="fa fa-book"></i> Inventario</a>
                            </li>
                        </ul>


                    </li>
                     <li id="vencidas">
                        <a href="vencidos.php"> <i class="fa fa-exclamation-triangle"></i> Medicamentos vencidos</a>
                    </li>
                    <li id="ventas">
                        <a href="ventas.php"><span class="" aria-hidden="true"></span> <i class="fa fa-shopping-cart"></i> Ventas</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>