<?php 
session_start();


if (!isset($_SESSION['usuario'])){

header("location:../");

}


 ?>


<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Inicio - Sistema de Inventario Farmacéutico </title>


    <!-- Bootstrap Core CSS -->
    <link href="../lib/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../lib/css/bootstrap-theme.min.css">

    <!-- Custom CSS -->
    <link href="../lib/css/sb-admin.css" rel="stylesheet">


    <!-- Custom Fonts -->
  <script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>




<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <?php include('includes/navbar.php');?>

        <div id="page-wrapper">
    <div class="imagen">
                        <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header" style="color:black">

                             Bienvenid@ <small style="color:black"><?php   echo $_SESSION['usuario']; ?> </small>
                       </h1>

                    </div>

                </div>
    </div>
 
               
            </div>

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->


<!-- PHP -->
<?php include('modals/cambiar_pass.php'); ?>
<!-- PHP -->
    <script type="text/javascript" src="../lib/js/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="../lib/js/jquery-1.12.3.js"></script>
    <script type="text/javascript" src="../lib/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/ctrlLogin.js"></script>

</body>

</html>
