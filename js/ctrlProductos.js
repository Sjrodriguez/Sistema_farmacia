// op = 1 agregar categoria
// op = 2 mostrar categoria
// op = 3 buscar categoria
// op = 4 eliminar categoria

// op = 5 agregar producto
// op = 6 mostrar producto
// op = 7 traer producto
// op = 8 modificar producto
// op = 9 eliminar producto
// op = 10 buscar producto
// op = 11 agregar paquete en Paquetes
// op = 12 mostrar paquetes en  Paquetes
// op = 13 eliminar paquete de Paquetes
// op = 14 buscar paquetes en  Paquetes
// op = 15 mostrar inventario
// op = 16 buscar en inventario
// op = 17 mostrar paquetes vencidos
// op = 18 buscar paquetes vencidos

$(document).ready(function(){

	mostrar_categorias()
	mostrar_productos()
	mostrar_paquetes_productos()
	mostrar_Inventario()
mostrar_Vencidos()

	mostrar_categorias_select()
	mostrar_productos_select()

	// Categorias
	$('body').on('click','#boton_agregar_cat',function(){

		var campos=["#nombre_cat","#descripcionn_cat"]
		var nombre=$("#nombre_cat").val()
		var descripcion=$("#descripcionn_cat").val()
		if(rt_vality(campos) == undefined){
			$.ajax({
				url:'../php/controller/ctrlProducto.php',
				type:'post',
				data:{
					'op':1,
					'nombre':nombre,
					'descripcion':descripcion
				},success:function(x){
					if(x == 1){
						alert("Se ha agregado una categoria")
					}else{
						alert("Error al agregar categoria")					
					}
					$("#nombre_cat").val('')
					$("#descripcionn_cat").val('')
					mostrar_categorias()
					mostrar_categorias_select()
				}
			})
		}else{
			$(rt_vality(campos)).focus();
			$(rt_vality(campos)).css({'background':'rgb(255, 227, 227)'});
		}

	})

	$('body').on('click','.btn_eliminar_categoria',function(){
		var id=$(this)[0].id

	var confir=confirm("Desea eliminar esta categoria?")
		if(confir){
				$.ajax({
					url:'../php/controller/ctrlProducto.php',
					type:'post',			
					data:{
						'op':4,
						'id_cat':id
					},success:function(x){
						if(x == 1){
							alert("Se ha eliminado correctamente")
						}else{
							alert("Error al eliminar categoria")
						}
						mostrar_categorias()
						mostrar_categorias_select()
					}
				})
		}else{
			alert("Su categoria esta segura")
		}

	})

// Productos

	$('body').on('click','#boton_agregar_pro',function(){

		var campos=['#codigo_pro','#nombre_pro','#precio_pro','#unidad_pro','#categoria_pro']
		var codigo=$("#codigo_pro").val()
		var nombre=$("#nombre_pro").val()
		var precio=$("#precio_pro").val()
		var unidad=$("#unidad_pro").val()
		var descripcion=$("#descripcion_pro").val()
		var categoria=$("#categoria_pro").val()
		if(descripcion == ""){
			descripcion="NO DISPONIBLE"
		}

		if(rt_vality(campos) == undefined){
			$.ajax({
				url:'../php/controller/ctrlProducto.php',
				type:'post',
				data:{
					'op':5,
					'codigo':codigo,
					'nombre':nombre,
					'precio':precio,
					'unidad':unidad,
					'descripcion':descripcion,
					'cod_cat':categoria
				},success:function(x){
					if(x == 1){
						alert("Se ha agregado un producto")
					}else{
						alert("Error al agregar un producto")
					}
					$("#codigo_pro").val("")
					$("#nombre_pro").val("")
					$("#precio_pro").val("")
					$("#unidad_pro").val("")
					$("#descripcion_pro").val("")
					$("#categoria_pro").val("")
					mostrar_productos()
				}		
			})
		}else{
			$(rt_vality(campos)).focus();
			$(rt_vality(campos)).css({'background':'rgb(255, 227, 227)'});
		}
	})

	$('body').on('click','.btn_modificar_producto',function(){

		var id=$(this)[0].id
		$('#id_pro_o').val(id)
		$.ajax({
			url:'../php/controller/ctrlProducto.php',
			type:'post',	
			data:{
				'op':7,
				'id_pro':id
			},success:function(data){
					data=eval(data)
					$("#codigo_editar_pro").val(data[0].codigo_pro)
					$("#nombre_editar_pro").val(data[0].nombre_pro)
					$("#precio_editar_pro").val(data[0].precio_pro)
					$("#unidad_editar_pro").val(data[0].unidad_pro)
					$("#descripcion_editar_pro").val(data[0].descripcion_pro)
					$("#categoria_editar_pro").val(data[0].cod_cat)
					mostrar_productos()
			}
		})

	})


	$('body').on('click','#boton_modificar_producto',function(){
		var campos=["#codigo_editar_pro","#nombre_editar_pro","#precio_editar_pro","#unidad_editar_pro",
		"#descripcion_editar_pro","#categoria_editar_pro"]
		var  id=$('#id_pro_o').val()
		var  codigo_edi = $("#codigo_editar_pro").val()
		var  nombre_edi = $("#nombre_editar_pro").val()
		var  precio_edi = $("#precio_editar_pro").val()
		var  unidad_edi = $("#unidad_editar_pro").val()
		var  descripcion_edi = $("#descripcion_editar_pro").val()
		var  cod_cat_edi = $("#categoria_editar_pro").val()

		if(rt_vality(campos) == undefined){
			$.ajax({
				url:'../php/controller/ctrlProducto.php',
				type:'post',
				data:{
					'op':8,
					'id':id,
					'codigo_edi':codigo_edi,
					'nombre_edi':nombre_edi,
					'precio_edi':precio_edi,
					'unidad_edi':unidad_edi,
					'descripcion_edi':descripcion_edi,
					'cod_cat_edi':cod_cat_edi
				},success:function(x){
					if(x == 1){
						alert("Se ha modificado correctamente")
					}else{
						alert("Error al modificar producto")
					}
					mostrar_productos()
				}	
			})


		}else{
			$(rt_vality(campos)).focus();
			$(rt_vality(campos)).css({'background':'rgb(255, 227, 227)'});			
		}

	})


	$('body').on('click','.btn_eliminar_producto',function(){

		var id_pro=$(this)[0].id
		var confir=confirm("Desea eliminar este producto?")

		if(confir){

			$.ajax({
				url:'../php/controller/ctrlProducto.php',
				type:'post',
				data:{
					'op':9,
					'id_pro':id_pro
				},success:function(x){
					if(x == 1){
						alert("Se ha eliminado correctamente")
					}else{
						alert("Error al eliminar producto")
					}
					mostrar_productos()
				}
			})
		}else{
			alert("Su producto esta seguro")
		}


	})




$('body').on('click','#btn_agregar_inventario',function(){


	var campos=['#producto_inv','#cantidad_inv','#fabricante_inv','#fabricado_inv','#comprado_inv','#vence_inv']
	var producto=$('#producto_inv').val()
	var cantidad=$('#cantidad_inv').val()
	var fabricante=$('#fabricante_inv').val()
	var fabricado=fecha_formato($('#fabricado_inv').val())
	var comprado=fecha_formato($('#comprado_inv').val())
	var vence=fecha_formato($('#vence_inv').val())
	var precio=$('#precio_inv').val()
	

	if(rt_vality(campos) == undefined){
		$.ajax({
			url:'../php/controller/ctrlProducto.php',
			type:'post',
			data:{
				'op':11,
				'producto':producto,
				'cantidad':cantidad,
				'fabricante':fabricante,
				'fabricado':fabricado,
				'comprado':comprado,
				'vence':vence,
				'precio':precio
			},success:function(x){
				if(x == 1){
					alert("Se ha agregado correctamente al inventario")
					$('#producto_inv').val("")
					$('#cantidad_inv').val("")
					$('#fabricante_inv').val("")
					$('#fabricado_inv').val("")
					$('#comprado_inv').val("")
					$('#vence_inv').val("")
					$('#precio_inv').val("")

				}else{
					alert("Error al agregar al inventario")
				}
				mostrar_paquetes_productos()
			}			
		})
	}else{
		$(rt_vality(campos)).focus();
		$(rt_vality(campos)).css({'background':'rgb(255, 227, 227)'});

	}

})

	$('body').on('click','.btn_eliminar_inventario',function(){
		var id_inv=$(this)[0].id
		var confir=confirm("Desea eliminar este paquete?")
		if(confir){
			$.ajax({
				url:'../php/controller/ctrlProducto.php',
				type:'post',
				data:{
					'op':13,
					'id_inv':id_inv
				},success:function(x){
					if(x == 1){
						alert("Se ha eliminado correctamente")
					}else{
						alert("Error al eliminar paquete de producto")
					}

					mostrar_paquetes_productos()
				}		
			})	
		}else{
			alert("Su paquete esta seguro")
		}


	})


})



// Funciones


function mostrar_categorias(){
	$.ajax({
		url:'../php/controller/ctrlProducto.php',
		type:'post',
		data:{
			'op':2
		},success:function(data){
			var html=""
			data=eval(data)

			for(a in data){
				html+="<tr  align='center' >";
				html+="<td>"+data[a].nombre_cat+"</td>";
				html+="<td>"+data[a].descripcion_cat+"</td>";
				html+="<td><button id="+data[a].cod_cat+" class='btn btn-danger btn_eliminar_categoria'> <i class='fa fa-trash-alt'></i></button></td>";

				html+="</tr>"
			}
			if(data.length == 0){
				html="<tr><td colspan='3'align='center'><h3>NO HAY CATEGORIAS EXISTENTES</h3></td></tr>"
			}

			$(".mostrar_categorias").html(html)
		}
	})

}


function buscar_categoria(){
	var texto=$('#buscar_cat').val()

	if(texto == ""){
		mostrar_categorias()
	}else{

		$.ajax({
			url:'../php/controller/ctrlProducto.php',
			type:'post',
			data:{
				'op':3,
				'texto':texto
			},success:function(data){
				var html=""
				data=eval(data)
				     		
				for(a in data){
					html+="<tr  align='center' >";
					html+="<td>"+data[a].nombre_cat+"</td>";
					html+="<td>"+data[a].descripcion_cat+"</td>";
					html+="<td><button id="+data[a].cod_cat+" class='btn btn-danger btn_eliminar_categoria'> <i class='fa fa-trash-alt'></i></button></td>";

					html+="</tr>"
				}
				if(data.length == 0){
					html="<tr><td colspan='3'align='center'><h3>NO HAY CATEGORIAS EXISTENTES</h3></td></tr>"
				}

				$(".mostrar_categorias").html(html)



			}
		})
	}

}

// PRODUCTOS

	function mostrar_productos(){
		$.ajax({
			url:'../php/controller/ctrlProducto.php',
			type:'post',
			data:{
				'op':6
			},success:function(data){
				var html=""
				data=eval(data)

				for(a in data){
					var precio=new Intl.NumberFormat().format(data[a].precio_pro)
					html+="<tr>";
					html+="<td>"+data[a].codigo_pro+"</td>";
					html+="<td>"+data[a].nombre_pro+"</td>";
					html+="<td>"+data[a].nombre_cat+"</td>";
					html+="<td>"+data[a].unidad_pro+"</td>";
					html+="<td>"+data[a].descripcion_pro+"</td>";
					html+="<td>"+"$"+precio+"</td>";
					html+="<td align='center' ><button class='btn btn-secondary btn_modificar_producto' data-toggle='modal' data-target='#modificar_producto' id="+data[a].cod_pro+"> <i class='fa fa-edit'></i></button> <button id="+data[a].cod_pro+" class='btn btn-danger btn_eliminar_producto'> <i class='fa fa-trash-alt'></i></button></td></tr>";

					html+="</tr>";				
				}

				if(data.length == 0){
					html="<tr><td colspan='7'align='center'><h3>NO HAY PRODUCTOS EXISTENTES</h3></td></tr>"
				}
				$(".mostrar_productos").html(html)

			}
		})
	}


	function mostrar_categorias_select(){
		$.ajax({
			url:'../php/controller/ctrlProducto.php',
			type:'post',
			data:{
				'op':2
			},success:function(data){
				var html=""
				data=eval(data)
					html+="<option value=''>-- Seleccione -- </option>"	
				for(a in data){
					html+="<option value='"+data[a].cod_cat+"'>"+data[a].nombre_cat+"</option>";
				}

				$(".categorias_input").html(html)
			}
		})

	}

	function buscar_Producto(){


		var buscarPor=$('#tip_cat').val()
		if(buscarPor == ""){
			buscarPor=0
		}
		var texto=$('#buscar_prod').val()
		if(texto == ""){
			mostrar_productos()
		}else{
			$.ajax({
				url:'../php/controller/ctrlProducto.php',
				type:'post',
				data:{
					'op':10,
					'texto':texto,
					'buscar':buscarPor
				},success:function(data){
					var html=""
					data=eval(data)

					for(a in data){
						var precio=new Intl.NumberFormat().format(data[a].precio_pro)
						html+="<tr>";
						html+="<td>"+data[a].codigo_pro+"</td>";
						html+="<td>"+data[a].nombre_pro+"</td>";
						html+="<td>"+data[a].nombre_cat+"</td>";
						html+="<td>"+data[a].unidad_pro+"</td>";
						html+="<td>"+data[a].descripcion_pro+"</td>";
						html+="<td>"+"$"+precio+"</td>";
						html+="<td align='center' ><button class='btn btn-secondary btn_modificar_producto' data-toggle='modal' data-target='#modificar_producto' id="+data[a].cod_pro+"> <i class='fa fa-edit'></i></button> <button id="+data[a].cod_pro+" class='btn btn-danger btn_eliminar_producto'> <i class='fa fa-trash-alt'></i></button></td></tr>";

						html+="</tr>";				
					}
					if(data.length == 0){
						html="<tr><td colspan='7'align='center'><h3>NO HAY PRODUCTOS EXISTENTES</h3></td></tr>"
					}
					$(".mostrar_productos").html(html)

				}	

			})			
		}


	}



	function mostrar_productos_select(){
		$.ajax({
			url:'../php/controller/ctrlProducto.php',
			type:'post',
			data:{
				'op':6
			},success:function(data){
				var html=""
				data=eval(data)
					html+="<option value=''> --- Seleccione producto --- </option>"				
				for(a in data){
					html+="<option value='"+data[a].cod_pro+"'>"+data[a].nombre_pro+"</option>"				
				}

				$(".producto_input").html(html)

			}
		})
	}





	function mostrar_paquetes_productos(){
		$.ajax({
			url:'../php/controller/ctrlProducto.php',
			type:'post',
			data:{
				'op':12
			},success:function(data){
				var html=""

				data=eval(data)

				for(a in data){
					var precio=new Intl.NumberFormat().format(data[a].precio_inv)

					html+="<tr align='center'>";
					html+="<td>"+data[a].codigo_pro+"<t/d>";
					html+="<td>"+data[a].nombre_pro+"<t/d>";
					html+="<td>"+data[a].nombre_cat+"<t/d>";
					html+="<td>"+data[a].fabricante_inv+"<t/d>";
					html+="<td>"+data[a].fabricado_inv+"<t/d>";
					html+="<td>"+data[a].comprado_inv+"<t/d>";
					html+="<td> $"+precio+"<t/d>";
					html+="<td>"+data[a].cantidad_inv+"<t/d>";
					html+="<td>"+data[a].vence_inv+"<t/d>";
					html+="<td> <button id="+data[a].cod_inv+" class='btn btn-danger btn_eliminar_inventario'> <i class='fa fa-trash-alt'></i></button></td>";
					html+="</tr>";

				}
					if(data.length == 0){
						html="<tr><td colspan='10'align='center'><h3>NO HAY PAQUETES DE PRODUCTOS EXISTENTES</h3></td></tr>"
					}

				$('.mostrar_paquetes').html(html)
			}
		})
	}


	function buscar_Paquetes(){
		var texto=$("#buscar_Pack").val()

		if(texto == ""){
			mostrar_paquetes_productos()
		}else{
			$.ajax({
				url:'../php/controller/ctrlProducto.php',
				type:'post',
				data:{
					'op':14,
					'texto':texto
				},success:function(data){
					var html=""

					data=eval(data)

					for(a in data){
						var precio=new Intl.NumberFormat().format(data[a].precio_inv)

						html+="<tr align='center'>";
						html+="<td>"+data[a].codigo_pro+"<t/d>";
						html+="<td>"+data[a].nombre_pro+"<t/d>";
						html+="<td>"+data[a].nombre_cat+"<t/d>";
						html+="<td>"+data[a].fabricante_inv+"<t/d>";
						html+="<td>"+data[a].fabricado_inv+"<t/d>";
						html+="<td>"+data[a].comprado_inv+"<t/d>";
						html+="<td> $"+precio+"<t/d>";
						html+="<td>"+data[a].cantidad_inv+"<t/d>";
						html+="<td>"+data[a].vence_inv+"<t/d>";
						html+="<td> <button id="+data[a].cod_inv+" class='btn btn-danger btn_eliminar_inventario'> <i class='fa fa-trash-alt'></i></button></td>";
						html+="</tr>";

					}
						if(data.length == 0){
							html="<tr><td colspan='10'align='center'><h3>NO HAY PAQUETES DE PRODUCTOS EXISTENTES</h3></td></tr>"
						}

					$('.mostrar_paquetes').html(html)
				}
			})			
		}

	}

	function mostrar_Inventario(){
		$.ajax({
			url:'../php/controller/ctrlProducto.php',
			type:'post',
			data:{
				'op':15
			},success:function(data){
				var html=""
				data=eval(data)

				for(x in data){
					var dataSplit=data[x].split("-")
					var precio=new Intl.NumberFormat().format(dataSplit[3])

					html+="<tr align='center'>";
					html+="<td>"+dataSplit[0]+"</td>"
					html+="<td>"+dataSplit[1]+"</td>"
					html+="<td>"+dataSplit[2]+"</td>"
					html+="<td> $"+precio+"</td>"
					html+="<td>"+dataSplit[4]+"</td>"
					html+="</tr>";

				}
					if(data.length == 0){
						html="<tr><td colspan='5'align='center'><h3>NO HAY  PRODUCTOS EN EL INVENTARIO</h3></td></tr>"
					}				
				$('.mostrar_inventario').html(html)

			}
		})
	}


	function buscar_Inventario(){
		var texto=$("#buscar_Inv").val()

		if(texto == ""){
			mostrar_Inventario()
		}else{
			$.ajax({
				url:'../php/controller/ctrlProducto.php',
				type:'post',
				data:{
					'op':16,
					'texto':texto
				},success:function(data){
					var html=""
					data=eval(data)

					for(x in data){
						var dataSplit=data[x].split("-")
						var precio=new Intl.NumberFormat().format(dataSplit[3])

						html+="<tr align='center'>";
						html+="<td>"+dataSplit[0]+"</td>"
						html+="<td>"+dataSplit[1]+"</td>"
						html+="<td>"+dataSplit[2]+"</td>"
						html+="<td> $"+precio+"</td>"
						html+="<td>"+dataSplit[4]+"</td>"
						html+="</tr>";

					}
						if(data.length == 0){
							html="<tr><td colspan='5'align='center'><h3>NO HAY  PRODUCTOS EN EL INVENTARIO</h3></td></tr>"
						}				
					$('.mostrar_inventario').html(html)

				}
			})			
		}
	}

	function mostrar_Vencidos(){
		$.ajax({
			url:'../php/controller/ctrlProducto.php',
			type:'post',
			data:{
				'op':17
			},success:function(data){
				var html=""
				data=eval(data)
				var contar=0
				for(x in data){
					if(rangoDias(fechaActual(),data[x].vence_inv) < 0){
						contar++
						var precio=new Intl.NumberFormat().format(data[x].precio_inv)
						html+="<tr align='center'>"
						html+="<td>"+data[x].codigo_pro+"</td>"
						html+="<td>"+data[x].nombre_pro+"</td>"
						html+="<td> $"+precio+"</td>"
						html+="<td>"+data[x].cantidad_inv+"</td>"
						html+="<td>"+data[x].vence_inv+"</td>"
						html+="</tr>"					
					}
				}

				if(contar == 0){
					html="<tr><td colspan='5'align='center'><h3>NO HAY  PRODUCTOS EN VENCIMIENTO</h3></td></tr>"
				}

				$('.mostrar_Venc').html(html)

			}
		})
	}


	function buscar_Vencidos(){

		var texto=$("#buscar_Ven").val()
		if(texto == ""){
			mostrar_Vencidos()
		}else{
			$.ajax({
				url:'../php/controller/ctrlProducto.php',
				type:'post',			
				data:{
					'op':18,
					'texto':texto
				},success:function(data){
					var html=""
					data=eval(data)
					var contar=0
					for(x in data){
						if(rangoDias(fechaActual(),data[x].vence_inv) < 0){
							contar++
							var precio=new Intl.NumberFormat().format(data[x].precio_inv)
							html+="<tr align='center'>"
							html+="<td>"+data[x].codigo_pro+"</td>"
							html+="<td>"+data[x].nombre_pro+"</td>"
							html+="<td> $"+precio+"</td>"
							html+="<td>"+data[x].cantidad_inv+"</td>"
							html+="<td>"+data[x].vence_inv+"</td>"
							html+="</tr>"					
						}
					}

					if(contar == 0){
						html="<tr><td colspan='5'align='center'><h3>NO HAY  PRODUCTOS EN VENCIMIENTO</h3></td></tr>"
					}

					$('.mostrar_Venc').html(html)

				}	
			})
		}
	}




// Funcioness

function rt_vality(x){
	// un fondo blanco a los inputs para limpiar
    $('input,textarea,select').css('background','white');
    // ---

    for(a in x){
      // retornar la clase que esta vacia
      if($(x[a]).val() ==""){
        return x[a];
      }
    }
}

function fecha_formato(fech){
	var fecha= fech.split("-")
	return fecha[2]+"/"+fecha[1]+"/"+fecha[0]
}
  function  rangoDias(desde, hasta){
	      var fecha1
	      var fecha2
	      var diasDif
	      var dias
	      var dia1=desde.split("/")[0]
	      var mes1=desde.split("/")[1] - 1
	      var yyy1=desde.split("/")[2]
	      var dia2=hasta.split("/")[0]
	      var mes2=hasta.split("/")[1] - 1
	      var yyy2=hasta.split("/")[2]
	     fecha1 = new Date(yyy1, mes1, dia1)
	     fecha2 = new Date(yyy2, mes2, dia2)
	     diasDif = fecha2.getTime() - fecha1.getTime();
	     dias = Math.round(diasDif/(1000 * 60 * 60 * 24));
		return dias
    }

 function fechaActual(){
    var hoy = new Date();
      //Fecha
      var dd = hoy.getDate();
      var mm = hoy.getMonth()+1; //hoy es 0!
      var yyyy = hoy.getFullYear();
    if(dd<10) {
        dd='0'+dd
    }
    if(mm<10) {
        mm='0'+mm
    }
    var  actual = dd+'/'+mm+'/'+yyyy;
    return actual
}
