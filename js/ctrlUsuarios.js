$(document).ready(function(){
 	$('#usuario').addClass("active")
	mostrar_usuarios()
			$(".ocult_tipo").hide()
					
	if(localStorage.getItem('tipo_usu')){
		if(localStorage.getItem('tipo_usu') == 1){
			$(".ocult_tipo").show()
		}
	}

	$('body').on('click','#boton_agregar_usu',function(){

	var campos=["#nombre_usu","#apellido_usu","#telefono_usu","#usuario_usu","#contrasena_usu","#confirmar_contra_usu","#tipo_usu"]
	var nombre=$("#nombre_usu").val();
	var apellido=$("#apellido_usu").val();
	var telefono=$("#telefono_usu").val();
	var usuario=$("#usuario_usu").val();
	var pass=$("#contrasena_usu").val();
	var confirm_pass=$("#confirmar_contra_usu").val();
	var direccion=$("#direccion_usu").val();
	var tipo=$("#tipo_usu").val();
	if(direccion == ""){
		direccion="NO DISPONIBLE"
	}


	if(rt_vality(campos) == undefined){
		if(pass == confirm_pass){
			$.ajax({
				url:'../php/controller/ctrlUsuarios.php',
				type:'post',
				data:{
					'op':2,
					'nombre':nombre,
					'apellido':apellido,
					'telefono':telefono,
					'nick':usuario,
					'pass':pass,
					'direccion':direccion,
					'tipo':tipo
				},success:function(x){
					if(x == 1){
						alert("Se ha agregado correctamente")
					}else{
						alert("Se ha producido un error al agregar")
					}
					$("#nombre_usu").val("");
					$("#apellido_usu").val("");
					$("#telefono_usu").val("");
					$("#usuario_usu").val("");
					$("#contrasena_usu").val("");
					$("#confirmar_contra_usu").val("");
					$("#direccion_usu").val("");
					$("#tipo_usu").val("");
					mostrar_usuarios()
				}		
			})



		}else{
			alert("Las contrasena no coinciden")
		$("#contrasena_usu").val("");
		$("#confirmar_contra_usu").val("");
		$("#contrasena_usu").focus();
		$("#contrasena_usu").css({'background':'rgb(255, 227, 227)'});
		}
	}else{
		$(rt_vality(campos)).focus();
		$(rt_vality(campos)).css({'background':'rgb(255, 227, 227)'});


	}




	})


// Modificar USUARIO
	$('body').on('click','.btn_modificar_usuario',function(){

	 var id = $(this)[0].id;
	 $('#id_usu_o').val(id);
	  		$.ajax({
				url:'../php/controller/ctrlUsuarios.php',
				type:'post',
				data:{
					'op':3,
					'id':id
				},success:function(data){
					data=eval(data)
					$('#nombre_editar_usu').val(data[0].nombre_usu)
					$('#apellido_editar_usu').val(data[0].apellido_usu)
					$('#telefono_editar_usu').val(data[0].telefono_usu)
					$('#usuario_editar_usu').val(data[0].nick_usu)
					$('#direccion_editar_usu').val(data[0].direccion_usu)
					$('#tipo_editar_usu').val(data[0].tipo_usu)
				}

	 		})
	})


	$('body').on('click','#boton_modificar_usu',function(){
	 	var id =$('#id_usu_o').val();
	 	var campos=['#nombre_editar_usu','#apellido_editar_usu','#telefono_editar_usu','#usuario_editar_usu',
	 	'#direccion_editar_usu','#tipo_editar_usu']
		var nombre_usu=$('#nombre_editar_usu').val()
		var apellido_usu=$('#apellido_editar_usu').val()
		var telefono_usu=$('#telefono_editar_usu').val()
		var nick_usu=$('#usuario_editar_usu').val()
		var direccion_usu=$('#direccion_editar_usu').val()
		var tipo_usu=$('#tipo_editar_usu').val()


		if(rt_vality(campos) == undefined){

			$.ajax({
				url:'../php/controller/ctrlUsuarios.php',
				type:'post',
				data:{
					'op':4,
					'id_edi':id,
					'nombre_edi':nombre_usu,
					'apellido_edi':apellido_usu,
					'telefono_edi':telefono_usu,
					'nick_edi':nick_usu,
					'direccion_edi':direccion_usu,
					'tipo_edi':tipo_usu
				},success:function(x){
					if(x == 1){
						alert('Se ha modificado correctamente')
					}else{
						alert('Se ha producido un error al modificar')
					}
					mostrar_usuarios()
				}
			})


		}else{
			$(rt_vality(campos)).focus();
			$(rt_vality(campos)).css({'background':'rgb(255, 227, 227)'});
		}

	})

// Eliminar Usuario

	$('body').on('click','.btn_eliminar_usuario',function(){

		 var id = $(this)[0].id;
		var confir=confirm("Desea eliminar este usuario?")
		if(confir){
			
			 $.ajax({
				  url:'../php/controller/ctrlUsuarios.php',
				  type:'post',
				  data:{
				  	'op':5,
				  	'id':id
				  },
				  success:function(x){
				  	if(x == 1){
				  		alert("Se ha eliminado correctamente")
				  	}else if(x == 2){
				  		alert("Se ha producido un error al eliminar")
				  	}else if(x == 3){
				  		alert("No puedes eliminar tu propio usuario")
				  	}
				  	mostrar_usuarios()
				  }
			 })

		}else{
			alert("Su usuario esta seguro")
		}

	 })





})


function mostrar_usuarios(){

	$.ajax({
		url:'../php/controller/ctrlUsuarios.php',
		type:'post',
		data:{
			'op':1
		},success:function(data){
			var html=""
			var tipo=""
			data = eval(data)


			for(a in data){

					if(data[a].tipo_usu == 1){
						tipo="Administrador"
					}else{
						tipo="Normal"
					}

					html+="<tr>";
					html+="<td>"+data[a].nombre_usu+' '+data[a].apellido_usu+"</td>";
					html+="<td>"+data[a].telefono_usu+"</td>";
					html+="<td>"+data[a].nick_usu+"</td>";
					html+="<td>"+data[a].direccion_usu+"</td>";
					html+="<td>"+tipo+"</td>";
					html+="<td align='center' ><button class='btn btn-secondary btn_modificar_usuario' data-toggle='modal' data-target='#modificar_usuario' id="+data[a].cod_usu+"> <i class='fa fa-edit'></i></button> <button id="+data[a].cod_usu+" class='btn btn-danger btn_eliminar_usuario'> <i class='fa fa-trash-alt'></i></button></td></tr>";
					html+="</tr>";


			}
			if(data.length == 0){
				html="<tr><td colspan='6'align='center'><h3>NO HAY USUARIOS EXISTENTES</h3></td></tr>"
			}

			$(".mostrar_usuario").html(html)
		}
	})
}

function buscador(){
	texto=$("#buscar_usuario").val()
	if(texto == ""){
		mostrar_usuarios()
	}else{
		$.ajax({
			url:'../php/controller/ctrlUsuarios.php',
			type:'post',
			data:{
				'op':6,
				'texto':texto
			},success:function(data){
				var html=""
				var tipo=""
				data = eval(data)
				for(a in data){

						if(data[a].tipo_usu == 1){
							tipo="Administrador"
						}else{
							tipo="Normal"
						}

						html+="<tr>";
						html+="<td>"+data[a].nombre_usu+' '+data[a].apellido_usu+"</td>";
						html+="<td>"+data[a].telefono_usu+"</td>";
						html+="<td>"+data[a].nick_usu+"</td>";
						html+="<td>"+data[a].direccion_usu+"</td>";
						html+="<td>"+tipo+"</td>";
						html+="<td align='center' ><button class='btn btn-secondary btn_modificar_usuario' data-toggle='modal' data-target='#modificar_usuario' id="+data[a].cod_usu+"> <i class='fa fa-edit'></i></button> <button id="+data[a].cod_usu+" class='btn btn-danger btn_eliminar_usuario'> <i class='fa fa-trash-alt'></i></button></td></tr>";
						html+="</tr>";


				}
				if(data.length == 0){
					html="<tr><td colspan='6'align='center'><h3>NO HAY USUARIOS EXISTENTES</h3></td></tr>"
				}

				$(".mostrar_usuario").html(html)
			}
		})
	}


}




function rt_vality(x){
	// un fondo blanco a los inputs para limpiar
    $('input,textarea,select').css('background','white');
    // ---

    for(a in x){
      // retornar la clase que esta vacia
      if($(x[a]).val() ==""){
        return x[a];
      }
    }
}
