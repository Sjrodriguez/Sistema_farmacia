<?php 
session_start();

if (isset($_SESSION['usuario'])){

header("location: paginas/home");
}


 ?>

<!DOCTYPE html>
<html >
  <head>
    <meta charset="UTF-8">
    <title>Sistema de Inventario Farmacéutico</title>
	
    
        <link rel="stylesheet" href="css/style.css">
        <!-- Font Awesome -->

  <script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>

    <!-- Boostrap Css -->


    
  </head>

  <body style="background: #262626;">

  
  
    <div class="container">

  <div id="login-form">

    <h3>Iniciar sesión</h3>

    <fieldset>

      <form action="">
        <input type="text" autofocus id="user" placeholder="Usuario"  autocomplete="off"> 

        <input type="password" onkeyup.enter="iniciar_Session()" id="pass" placeholder="Contraseña"  autocomplete="off">  
  
        <!-- <input  class="faambulance"  value="Ingresar"> -->
        <button type="button" onclick="iniciar_Session()" id="log" > <i class="fa fa-ambulance"></i> Ingresar </button>
      </form>

    </fieldset>

  </div> <!-- end login-form -->

</div>
    
    
    
<script type="text/javascript" src="lib/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="lib/js/jquery-1.12.3.js"></script>
<script type="text/javascript" src="lib/js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/ctrlLogin.js"></script>

    
  </body>
</html>



 