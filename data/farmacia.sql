-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 01-05-2018 a las 17:55:51
-- Versión del servidor: 10.1.31-MariaDB
-- Versión de PHP: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `farmacia`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `cod_cat` int(9) NOT NULL,
  `nombre_cat` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `descripcion_cat` varchar(60) COLLATE utf8_spanish2_ci NOT NULL,
  `estado_cat` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`cod_cat`, `nombre_cat`, `descripcion_cat`, `estado_cat`) VALUES
(1, 'Jabon', 'adsdasd', 2),
(2, 'asdasd', 'asdasasdasdasdasdasd', 2),
(3, 'Yuca', 'al lado', 2),
(4, 'ropa', 'pantalon jean', 2),
(5, 'Ropa', 'pantalones', 1),
(6, 'Comida', 'para comer', 1),
(7, 'Utencilios', 'cosas', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventario`
--

CREATE TABLE `inventario` (
  `cod_inv` int(9) NOT NULL,
  `cod_pro` int(9) NOT NULL,
  `vence_inv` varchar(17) COLLATE utf8_spanish2_ci NOT NULL,
  `fabricante_inv` varchar(60) COLLATE utf8_spanish2_ci NOT NULL,
  `fabricado_inv` varchar(17) COLLATE utf8_spanish2_ci NOT NULL,
  `cantidad_inv` int(9) NOT NULL,
  `comprado_inv` varchar(17) COLLATE utf8_spanish2_ci NOT NULL,
  `precio_inv` double NOT NULL,
  `estado_inv` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `inventario`
--

INSERT INTO `inventario` (`cod_inv`, `cod_pro`, `vence_inv`, `fabricante_inv`, `fabricado_inv`, `cantidad_inv`, `comprado_inv`, `precio_inv`, `estado_inv`) VALUES
(1, 6, '45555-05-04', 'asdasd', '0445-05-04', 1212, '0455-05-04', 100, 2),
(2, 6, '2018-02-01', 'malta', '2017-02-01', 10, '2018-05-20', 200, 1),
(3, 5, '03-05-2018', 'la ca;le', '01-01-2020', 50, '02-02-2019', 0, 1),
(4, 5, '21-02-1211', '222', '22-02-1111', 1, '12-02-12111', 0, 1),
(5, 5, '11-11-0012', '1111', '11-02-2018', 1122, '11-02-0112', 11, 1),
(6, 6, '03-02-2019', 'Joselito', '01-01-2018', 122, '02-03-2020', 2, 1),
(7, 5, '28/12/2014', 'jose', '30/03/2017', 20, '03/12/2015', 3, 1),
(8, 6, '01/01/2018', '1121', '02/11/2017', 100, '29/11/2017', 29, 1),
(9, 7, '02/02/2018', 'jose', '01/01/2018', 15, '03/01/2018', 3, 2),
(10, 7, '01/01/2018', '122', '02/01/2018', 10, '02/01/2016', 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `cod_pro` int(9) NOT NULL,
  `codigo_pro` varchar(60) COLLATE utf8_spanish2_ci NOT NULL,
  `cod_cat` int(9) NOT NULL,
  `nombre_pro` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `precio_pro` double NOT NULL,
  `descripcion_pro` varchar(80) COLLATE utf8_spanish2_ci NOT NULL,
  `unidad_pro` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `estado_pro` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`cod_pro`, `codigo_pro`, `cod_cat`, `nombre_pro`, `precio_pro`, `descripcion_pro`, `unidad_pro`, `estado_pro`) VALUES
(5, '0000', 5, 'Acetaminofen', 1000, 'tabletas', 'rojas', 1),
(6, '1110', 6, 'Yuca', 10, 'paquetes', 'nagua', 1),
(7, '000001', 7, 'no se ', 15, 'paquetes', 'prueba', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `cod_usu` int(9) NOT NULL,
  `nombre_usu` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `apellido_usu` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `estado_usu` int(1) NOT NULL DEFAULT '1',
  `nick_usu` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `pass_usu` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `telefono_usu` varchar(15) COLLATE utf8_spanish2_ci NOT NULL,
  `direccion_usu` varchar(60) COLLATE utf8_spanish2_ci NOT NULL,
  `tipo_usu` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`cod_usu`, `nombre_usu`, `apellido_usu`, `estado_usu`, `nick_usu`, `pass_usu`, `telefono_usu`, `direccion_usu`, `tipo_usu`) VALUES
(1, 'Yandra', 'Campusano', 1, 'admin', '1234', '1121', 'asdasdasdasdasd', 2),
(4, 'Manuel', 'De leon', 1, 'josee', '122', '15454', 'calle', 1),
(5, 'Jose', 'Ramon', 1, 'Joselito', '1234', '565655', 'asdasdas', 2),
(7, 'Edgar', 'Tavarez', 2, 'edgar004', '1234', '1110011', '1001', 2),
(8, 'admasdasd', 'adfsd', 1, 'dasda', '111', '111', '11', 1),
(9, 'Yandra', 'Campusano', 2, 'yandra15', '1234', '11111', 'una casa', 2);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`cod_cat`);

--
-- Indices de la tabla `inventario`
--
ALTER TABLE `inventario`
  ADD PRIMARY KEY (`cod_inv`),
  ADD KEY `cod_pro` (`cod_pro`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`cod_pro`),
  ADD KEY `cod_cat` (`cod_cat`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`cod_usu`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `cod_cat` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `inventario`
--
ALTER TABLE `inventario`
  MODIFY `cod_inv` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `cod_pro` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `cod_usu` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `inventario`
--
ALTER TABLE `inventario`
  ADD CONSTRAINT `inventario_ibfk_1` FOREIGN KEY (`cod_pro`) REFERENCES `productos` (`cod_pro`);

--
-- Filtros para la tabla `productos`
--
ALTER TABLE `productos`
  ADD CONSTRAINT `productos_ibfk_1` FOREIGN KEY (`cod_cat`) REFERENCES `categoria` (`cod_cat`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
