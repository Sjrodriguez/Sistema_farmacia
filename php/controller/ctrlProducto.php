<?php 
// op = 1 agregar categoria
// op = 2 mostrar categoria
// op = 3 buscar categoria
// op = 4 eliminar categoria

// op = 5 agregar producto
// op = 6 mostrar producto
// op = 7 traer producto
// op = 8 modificar producto
// op = 9 eliminar producto
// op = 10 buscar producto
// op = 11 agregar paquete en Paquetes
// op = 12 mostrar paquetes en  Paquetes
// op = 13 eliminar paquete de Paquetes
// op = 14 buscar paquetes en  Paquetes
// op = 15 mostrar inventario
// op = 16 buscar en inventario
// op = 17 mostrar paquetes vencidos
// op = 18 buscar paquetes vencidos

require_once('../core/data.php');
setlocale(LC_MONETARY, 'en_US');

$conn=new Data;


$op=$_POST['op'];



if($op == 1){
		session_start();
	$nombre_cat=$_POST['nombre'];
	$descripcion_cat=$_POST['descripcion'];

	$sql="INSERT INTO categoria (nombre_cat,descripcion_cat,estado_cat)VALUES('$nombre_cat','$descripcion_cat',1)";

	$resultado=$conn->guardar($sql);
	if($resultado){
		echo 1;
	}
}else if($op == 2){
		session_start();


	$sql="SELECT * FROM categoria WHERE estado_cat=1 ORDER BY cod_cat DESC";
	echo $conn->buscar($sql,"JSON");
}else if($op == 3){
			session_start();

	
	$texto=$_POST['texto'];
	$sql="SELECT * FROM categoria WHERE nombre_cat  LIKE '$texto%' AND estado_cat=1 ORDER BY cod_cat DESC";
	echo $conn->buscar($sql,"JSON");

}else if($op == 4){
				session_start();

	$id_cat=$_POST['id_cat'];
	$sql="UPDATE categoria SET estado_cat=2 WHERE cod_cat='$id_cat'";
	$resultado=$conn->modificar($sql);
	if($resultado){
		echo 1;
	}else{
		echo 2;
	}
}else if($op == 5){
				session_start();

	$codigo=$_POST['codigo'];
	$cod_cat=$_POST['cod_cat'];
	$nombre=$_POST['nombre'];
	$precio=money_format('%i', $_POST['precio']) . "\n";
	$unidad=$_POST['unidad'];
	$descripcion=$_POST['descripcion'];

	$sql="INSERT INTO productos (codigo_pro,cod_cat,nombre_pro,precio_pro,descripcion_pro,unidad_pro)VALUES('$codigo','$cod_cat','$nombre','$precio','$unidad','$descripcion')";

	$resultado=$conn->guardar($sql);
	if($resultado){
		echo 1;
	}else{
		echo 2;
	}
}else if($op == 6){
				session_start();

	$sql="SELECT * FROM productos INNER JOIN categoria ON productos.cod_cat=categoria.cod_cat WHERE productos.estado_pro=1 ORDER BY productos.cod_pro DESC";
	echo $conn->buscar($sql,"JSON");

}else if($op == 7){
				session_start();

	$id=$_POST['id_pro'];
	$sql="SELECT * FROM productos WHERE cod_pro='$id'";
	echo $conn->buscar($sql,"JSON");
}else if($op == 8){
				session_start();

	$id=$_POST['id'];
	$codigo_edi=$_POST['codigo_edi'];
	$nombre_edi=$_POST['nombre_edi'];
	$precio_edi=$_POST['precio_edi'];
	$unidad_edi=$_POST['unidad_edi'];
	$descripcion_edi=$_POST['descripcion_edi'];
	$cod_cat_edi=$_POST['cod_cat_edi'];


	$sql="UPDATE productos SET codigo_pro='$codigo_edi',nombre_pro='$nombre_edi',precio_pro='$precio_edi',unidad_pro='$unidad_edi',descripcion_pro='$descripcion_edi',cod_cat='$cod_cat_edi' WHERE  cod_pro='$id'" ;
	$resultado=$conn->modificar($sql);

	if($resultado){
		echo 1;
	}else{
		echo 2;
	}
}else if($op == 9){
				session_start();

	$id=$_POST['id_pro'];
	$sql="UPDATE productos SET estado_pro=2 WHERE cod_pro='$id'";
	$resultado=$conn->modificar($sql);
	if($resultado){
		echo 1;
	}else{
		echo 2;
	}

}else if($op == 10){
				session_start();

	$texto=$_POST['texto'];
	$buscar=$_POST['buscar'];

	if($buscar == 1 || $buscar == 0){
		$sql="SELECT * FROM productos INNER JOIN categoria ON productos.cod_cat=categoria.cod_cat WHERE productos.estado_pro=1 AND productos.codigo_pro LIKE '$texto%' ORDER BY productos.cod_pro DESC";
	}else if($buscar == 2){
		$sql="SELECT * FROM productos INNER JOIN categoria ON productos.cod_cat=categoria.cod_cat WHERE productos.estado_pro=1 AND productos.nombre_pro LIKE '$texto%' ORDER BY productos.cod_pro DESC";		
	}else if($buscar == 3){
		$sql="SELECT * FROM productos INNER JOIN categoria ON productos.cod_cat=categoria.cod_cat WHERE productos.estado_pro=1 AND categoria.nombre_cat LIKE '$texto%' ORDER BY productos.cod_pro DESC";
	}

	echo $conn->buscar($sql,"JSON");

}else if($op == 11){
				session_start();

	$cod_pro=$_POST['producto'];
	$vence_inv=$_POST['vence'];
	$fabricante_inv=$_POST['fabricante'];
	$fabricado_inv=$_POST['fabricado'];
	$cantidad_inv=$_POST['cantidad'];
	$comprado_inv=$_POST['comprado'];
	$precio_inv=$_POST['comprado'];

	$sql="INSERT INTO inventario ( cod_pro,vence_inv,fabricante_inv,fabricado_inv,cantidad_inv,comprado_inv,precio_inv )VALUES('$cod_pro','$vence_inv','$fabricante_inv','$fabricado_inv','$cantidad_inv','$comprado_inv','$precio_inv') ";
	$resultado=$conn->guardar($sql);
	if($resultado){
		echo 1;
	}else{
		echo 2;
	}
}else if($op == 12){
				session_start();

	$sql="SELECT inventario.cod_inv,productos.codigo_pro,productos.nombre_pro,categoria.nombre_cat,inventario.fabricante_inv,inventario.fabricado_inv,inventario.comprado_inv,inventario.fabricante_inv,inventario.fabricado_inv,inventario.comprado_inv,inventario.precio_inv,inventario.cantidad_inv,inventario.vence_inv FROM inventario INNER JOIN productos ON inventario.cod_pro=productos.cod_pro INNER JOIN categoria ON categoria.cod_cat=productos.cod_cat  WHERE inventario.estado_inv=1 ORDER BY inventario.cod_inv DESC";
	echo $conn->buscar($sql,"JSON");

}else if($op == 13){
				session_start();

	$id_inv=$_POST['id_inv'];

	$sql="UPDATE inventario SET estado_inv=2 WHERE cod_inv='$id_inv'";
	$resultado=$conn->modificar($sql);
	if($resultado){
		echo 1;
	}else{
		echo 2;
	}
}else if($op== 14){
				session_start();

	$texto=$_POST['texto'];
		$sql="SELECT inventario.cod_inv,productos.codigo_pro,productos.nombre_pro,categoria.nombre_cat,inventario.fabricante_inv,inventario.fabricado_inv,inventario.comprado_inv,inventario.fabricante_inv,inventario.fabricado_inv,inventario.comprado_inv,inventario.precio_inv,inventario.cantidad_inv,inventario.vence_inv FROM inventario INNER JOIN productos ON inventario.cod_pro=productos.cod_pro INNER JOIN categoria ON categoria.cod_cat=productos.cod_cat  WHERE inventario.estado_inv=1 AND productos.codigo_pro LIKE '$texto%' ORDER BY inventario.cod_inv DESC";
	echo $conn->buscar($sql,"JSON");

}else if($op == 15){
				session_start();

	$sql="SELECT productos.cod_pro,productos.codigo_pro,productos.nombre_pro,categoria.nombre_cat,productos.precio_pro FROM productos INNER JOIN categoria ON productos.cod_cat=categoria.cod_cat ";
	$result=$conn->buscar($sql);
	$long=count($result);
	$cant=0;
	$array=[];
	for ( $i = 0; $i < $long; $i++ ){

		$sql1="SELECT cod_pro,cantidad_inv FROM inventario WHERE estado_inv=1 AND cod_pro=".$result[$i][0]."  ";
		$result1=$conn->buscar($sql1);
		$long1=count($result1);;

// Aqeuiii
			for($x = 0; $x < $long1; $x++){
					$cant=$cant+ $result1[$x][1];

			}
			array_push($array,$result[$i][1]."-".$result[$i][2]."-".$result[$i][3]."-".$result[$i][4]."-".$cant);

		$cant=0;

	}
	echo json_encode($array);
	
}else if($op == 16){
				session_start();

$texto=$_POST['texto'];

$sql="SELECT productos.cod_pro,productos.codigo_pro,productos.nombre_pro,categoria.nombre_cat,productos.precio_pro FROM productos INNER JOIN categoria ON productos.cod_cat=categoria.cod_cat WHERE productos.codigo_pro LIKE '$texto%' OR productos.nombre_pro LIKE '$texto%'";
	$result=$conn->buscar($sql);
	$long=count($result);
	$cant=0;
	$array=[];
	for ( $i = 0; $i < $long; $i++ ){

		$sql1="SELECT cod_pro,cantidad_inv FROM inventario WHERE estado_inv=1 AND cod_pro=".$result[$i][0]." ";
		$result1=$conn->buscar($sql1);
		$long1=count($result1);;

// Aqeuiii
			for($x = 0; $x < $long1; $x++){
					$cant=$cant+ $result1[$x][1];

			}
			array_push($array,$result[$i][1]."-".$result[$i][2]."-".$result[$i][3]."-".$result[$i][4]."-".$cant);

		$cant=0;

	}
	echo json_encode($array);
	
}else if($op == 17){
					session_start();

$sql="SELECT productos.codigo_pro,productos.nombre_pro,inventario.cantidad_inv,inventario.precio_inv,inventario.vence_inv FROM inventario INNER JOIN productos ON productos.cod_pro=inventario.cod_pro ORDER BY inventario.cod_inv DESC";
	echo $conn->buscar($sql,'JSON');

}else if($op == 18){
					session_start();

$texto=$_POST['texto'];
	$sql="SELECT productos.codigo_pro,productos.nombre_pro,inventario.cantidad_inv,inventario.precio_inv,inventario.vence_inv FROM inventario INNER JOIN productos ON productos.cod_pro=inventario.cod_pro WHERE productos.codigo_pro LIKE '$texto%' OR productos.nombre_pro LIKE '$texto%' ORDER BY inventario.cod_inv DESC ";
	echo $conn->buscar($sql,'JSON');	
}




// 


